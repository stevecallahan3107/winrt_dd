﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace DentalDirectory
{
    public class CategoriesApiModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("product_count")]
        public int Count { get; set; }

        [JsonProperty("categories")]
        public List<CategoryApiModel> Categories { get; set; }
    }

    public class CategoryApiModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class SubCategoriesApiModel
    {
        [JsonProperty("category_name")]
        public string Name { get; set; }

        [JsonProperty("category_code")]
        public string Code { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("product_count")]
        public int Count { get; set; }

        [JsonProperty("sub_categories")]
        public List<SubCategoryApiModel> SubCategories { get; set; }
    }

    public class SubCategoryApiModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class BrandsApiModel
    {
        [JsonProperty("category_name")]
        public string Name { get; set; }

        [JsonProperty("category_code")]
        public string Code { get; set; }

        [JsonProperty("sub_category_name")]
        public string subCtgName { get; set; }

        [JsonProperty("sub_category_code")]
        public string subCtgCode { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("product_count")]
        public int Count { get; set; }

        [JsonProperty("brand_manufacturers")]
        public List<BrandApiModel> Brands { get; set; }
    }

    public class BrandApiModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class BrandsDetailApiModel
    {
        [JsonProperty("category_name")]
        public string Name { get; set; }

        [JsonProperty("category_code")]
        public string Code { get; set; }

        [JsonProperty("sub_category_name")]
        public string subCtgName { get; set; }

        [JsonProperty("sub_category_code")]
        public string subCtgCode { get; set; }

        [JsonProperty("brand_manufacturers_name")]
        public string manufName { get; set; }

        [JsonProperty("brand_manufacturers_code")]
        public string manufCode { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("product_count")]
        public int Count { get; set; }

        [JsonProperty("products")]
        public List<BrandDetailApiModel> BrandsDetail { get; set; }
    }

    public class BrandDetailApiModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        private string _code = null;
        [JsonProperty("code")]
        public string Code 
        { 
            get
            {
                return _code;
            }
            set
            {
                _code = value;
                CreateImageUrl();
            }
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("uom")]
        public string Uom { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("prescription")]
        public bool Prescription { get; set; }

        public Uri ImageRef { get; set; }

        private void CreateImageUrl()
        {
            var codeStr1 = Code.Substring(0, 1);
            var codeStr2 = Code.Substring(1, 1);
            var codeStr3 = Code.Substring(2, 1);
            string url = "https://www.dental-directory.co.uk/virtualimagedirectory/imagebin/" + codeStr1 + "/" + codeStr2 + "/" + codeStr3 + "/" + Code + "_Image310xMAX.jpg";
            try
            {
                Uri uri = new Uri(url);
                BitmapImage bitmap = new BitmapImage(uri);
                Image image = new Image();
                image.Source = bitmap;
            }
            catch(Exception e)
            {
                url = "http://btest.mip.local/Assets/Images/no_photo_Image310xMax.jpg";
            }
            ImageRef = new Uri(url);
        }
    }

    public class BrandItemDetailApiModel
    {
        [JsonProperty("is_authenticated")]
        public bool Authenticated { get; set; }

        [JsonProperty("category_name")]
        public string Name { get; set; }

        [JsonProperty("category_code")]
        public string Code { get; set; }

        [JsonProperty("sub_category_name")]
        public string subCtgName { get; set; }

        [JsonProperty("sub_category_code")]
        public string subCtgCode { get; set; }

        [JsonProperty("brand_manufacturers_name")]
        public string manufName { get; set; }

        [JsonProperty("brand_manufacturers_code")]
        public string manufCode { get; set; }

        [JsonProperty("product_name")]
        public string prodName { get; set; }

        [JsonProperty("product_code")]
        public string prodCode { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("description")]
        public string Desc { get; set; }

        [JsonProperty("uom")]
        public string Uom { get; set; }

        [JsonProperty("price_break_1")]
        public decimal priceBrk1 { get; set; }

        [JsonProperty("price_break_2")]
        public decimal priceBrk2 { get; set; }

        [JsonProperty("price_break_3")]
        public decimal priceBrk3 { get; set; }

        [JsonProperty("quantity_break_2")]
        public int qtyBrk2 { get; set; }

        [JsonProperty("quantity_break_3")]
        public int qtyBrk3 { get; set; }
    }

}
