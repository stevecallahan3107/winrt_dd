﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DentalDirectory
{
    public class MainModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private CategoriesApiModel _category;
        public CategoriesApiModel Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
                OnPropertyChanged("Category");
            }
        }

        private SubCategoriesApiModel _subcategory;
        public SubCategoriesApiModel SubCategory
        {
            get
            {
                return _subcategory;
            }
            set
            {
                _subcategory = value;
                OnPropertyChanged("SubCategory");
            }
        }

        private BrandsApiModel _brand;
        public BrandsApiModel Brand
        {
            get
            {
                return _brand;
            }
            set
            {
                _brand = value;
                OnPropertyChanged("Brand");
            }
        }

        private BrandsDetailApiModel _brandDtl;
        public BrandsDetailApiModel BrandDtl
        {
            get
            {
                return _brandDtl;
            }
            set
            {
                _brandDtl = value;
                OnPropertyChanged("BrandDtl");
            }
        }       
        
        private BrandItemDetailApiModel _brandItemDtl;
        public BrandItemDetailApiModel BrandItemDtl
        {
            get
            {
                return _brandItemDtl;
            }
            set
            {
                _brandItemDtl = value;
                OnPropertyChanged("BrandItemDtl");
            }
        }

        private string[] _searchList;
        public string[] SearchList
        {
            get
            {
                return _searchList;
            }
            set
            {
                _searchList = value;
                OnPropertyChanged("SearchList");
            }
        }

        public async Task ctgLoad()
        {
            JsonWebClient client = new JsonWebClient();
            string url = "http://api.mip.local/catalogue";
            string json = await client.Download(url);
            Category = (CategoriesApiModel)JsonConvert.DeserializeObject(json, typeof(CategoriesApiModel));
        }

        public async Task subCtgLoad(string code)
        {
            JsonWebClient client = new JsonWebClient();
            string url = "http://api.mip.local/catalogue/" + code;
            string json = await client.Download(url);
            SubCategory = (SubCategoriesApiModel)JsonConvert.DeserializeObject(json, typeof(SubCategoriesApiModel));
        }

        public async Task brandLoad(string code)
        {
            JsonWebClient client = new JsonWebClient();
            string url = "http://api.mip.local/catalogue/" + code;
            string json = await client.Download(url);
            Brand = (BrandsApiModel)JsonConvert.DeserializeObject(json, typeof(BrandsApiModel));
        }

        public async Task brandDtlLoad(string code)
        {
            JsonWebClient client = new JsonWebClient();
            string url = "http://api.mip.local/catalogue/" + code;
            string json = await client.Download(url);
            BrandDtl = (BrandsDetailApiModel)JsonConvert.DeserializeObject(json, typeof(BrandsDetailApiModel));
        }

        public async Task brandItemDtlLoad(string code)
        {
            JsonWebClient client = new JsonWebClient();
            string url = "http://api.mip.local/product/code/" + code;
            string json = await client.Download(url);
            BrandItemDtl = (BrandItemDetailApiModel)JsonConvert.DeserializeObject(json, typeof(BrandItemDetailApiModel));
        }

    }

    public class JsonWebClient
    {
        public async Task<string> Download(string url)
        {
           HttpClient http = new HttpClient();

            if (((App)(App.Current)).tokenVal != null)
            {
                http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("authorisation", "Bearer " + ((App)(App.Current)).tokenVal);
            }

            var response = await http.GetByteArrayAsync(url);
            String source = Encoding.GetEncoding("utf-8").GetString(response, 0, response.Length);

            return source;
        }
    }
}
