﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace DentalDirectory
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BrandItemDetail : Page
    {
        public MainModel Model { get; set; }
        public string gCode;
        public string gName;
        public BitmapImage imgView;

        public BrandItemDetail()
        {
            this.InitializeComponent();

            if (((App)(App.Current)).loggedIn)
            {
                UserBttn.Visibility = Visibility.Visible;
                PriceTxt.Visibility = Visibility.Collapsed;
                PriceVal.Visibility = Visibility.Collapsed;
                PriceHdr.Visibility = Visibility.Visible;
                QtyHdr.Visibility = Visibility.Visible;
                QtyHdr2.Visibility = Visibility.Visible;
                QtyHdr3.Visibility = Visibility.Visible;
                QtyHdr4.Visibility = Visibility.Visible;
                PricesHdr.Visibility = Visibility.Visible;
                PricesHdr2.Visibility = Visibility.Visible;
                PricesHdr3.Visibility = Visibility.Visible;
                PricesHdr4.Visibility = Visibility.Visible;
            }
            else
            {
                UserBttn.Visibility = Visibility.Collapsed;
            }

            this.NavigationCacheMode = NavigationCacheMode.Required;

            Model = new MainModel();
            DataContext = Model;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await Model.brandItemDtlLoad(gCode);
             manufTxt.Text = Model.BrandItemDtl.manufName;
             ctgTxt.Text = Model.BrandItemDtl.Name + " > " + Model.BrandItemDtl.subCtgName;
             CodeTxt.Text = gCode;
             UomTxt.Text = Model.BrandItemDtl.Uom;
             if (((App)(App.Current)).loggedIn)
             {
                 if (Model.BrandItemDtl.qtyBrk2 > 0)
                 {
                     if (Model.BrandItemDtl.qtyBrk2 > 0)
                     {
                         QtyHdr2.Text = "1 to " + Model.BrandItemDtl.qtyBrk2.ToString();
                     }
                     if (Model.BrandItemDtl.qtyBrk3 > 0)
                     {
                         QtyHdr3.Text = Model.BrandItemDtl.qtyBrk2.ToString() + " to " + Model.BrandItemDtl.qtyBrk3.ToString();
                     }
                     if (Model.BrandItemDtl.qtyBrk3 > 0)
                     {
                         QtyHdr4.Text = Model.BrandItemDtl.qtyBrk3.ToString() + " +";
                     }
                     if (Model.BrandItemDtl.priceBrk1 > 0)
                     {
                         PricesHdr2.Text = Model.BrandItemDtl.priceBrk1.ToString("G");
                     }
                     if (Model.BrandItemDtl.priceBrk2 > 0)
                     {
                         PricesHdr3.Text = Model.BrandItemDtl.priceBrk2.ToString("G");
                     }
                     if (Model.BrandItemDtl.priceBrk3 > 0)
                     {
                        PricesHdr4.Text = Model.BrandItemDtl.priceBrk3.ToString("G");
                     }
                 }
                 else
                 {
                     if (Model.BrandItemDtl.priceBrk1 > 0)
                     {
                         PriceVal.Text = Model.BrandItemDtl.priceBrk1.ToString("G");
                     }
                 }
             }
             else
             {
                 PriceVal.Text = "Sign on to view prices";
             }
             descText.Text = Model.BrandItemDtl.Desc;
             var codeStr1 = gCode.Substring(0, 1);
             var codeStr2 = gCode.Substring(1, 1);
             var codeStr3 = gCode.Substring(2, 1);
             string url = "https://www.dental-directory.co.uk/virtualimagedirectory/imagebin/" + codeStr1 + "/" + codeStr2 + "/" + codeStr3 + "/" + gCode + "_Image310xMAX.jpg";
             getImage(url);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var brandItemDetail = e.Parameter as BrandsDetail.BrandDtlClass;
            gCode = brandItemDetail.code;
            gName = brandItemDetail.name;
            brandItemTxt.Text = gName;
            ((App)(App.Current)).brandItemDtlHdr = gName;
        }

        private void MenuBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Menu));
        }

        private void SearchBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Search));
        }

        private async void getImage(String uri)
        {
            var httpClient = new HttpClient();
            imgView = new BitmapImage();                
            try
            {
                var contentBytes = await httpClient.GetByteArrayAsync(uri);
                var ims = new InMemoryRandomAccessStream();
                var dataWriter = new DataWriter(ims);
                dataWriter.WriteBytes(contentBytes);
                await dataWriter.StoreAsync();
                ims.Seek(0);

                imgView.SetSource(ims);
            }
            catch //(Exception err)
            {
                var imgPath = new Uri(this.BaseUri, "../Assets/BlankImg.jpg");
                imgView.UriSource = imgPath;
            }

            dtlImage.Source = imgView;       
        }

        private void MinusBttn_Click(object sender, RoutedEventArgs e)
        {
            var iQty = Int32.Parse(qtyTxt.Text);
            if (iQty > 1)
            {
                iQty--;
            }
            qtyTxt.Text = iQty.ToString();
        }

        private void PlusBttn_Click(object sender, RoutedEventArgs e)
        {
            var iQty = Int32.Parse(qtyTxt.Text);
            if (iQty < 99)
            {
                iQty++;
            }
            qtyTxt.Text = iQty.ToString();
        }

        private void addToCartBttn_Click(object sender, RoutedEventArgs e)
        {
            //go to Cart
        }

    }
}
