﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace DentalDirectory
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SubCategories : Page
    {
        public MainModel Model { get; set; }
        public string gCode;
        public string gName;

        public SubCategories()
        {
            this.InitializeComponent();

            if (((App)(App.Current)).loggedIn)
            {
                UserBttn.Visibility = Visibility.Visible;
            }
            else
            {
                UserBttn.Visibility = Visibility.Collapsed;
            }

            this.NavigationCacheMode = NavigationCacheMode.Required;

            Model = new MainModel();
            DataContext = Model;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await Model.subCtgLoad(gCode);
        }

        private void subCtgList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1)
            {
                SubCategoryApiModel ctgSelected = (SubCategoryApiModel)e.AddedItems[0];
                SubCategoryClass subCtgDtl = new SubCategoryClass();
                subCtgDtl.name = ctgSelected.Name;
                subCtgDtl.code = gCode + "/" + ctgSelected.Code;
                ((App)(App.Current)).subCategoryDtl_Name = subCtgDtl.name;
                ((App)(App.Current)).subCategoryDtl_Code = subCtgDtl.code;
                Frame.Navigate(typeof(Brands), subCtgDtl);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var ctgDetail = e.Parameter as MainPage.CategoryClass;
            gCode = ctgDetail.code;
            gName = ctgDetail.name;
            ((App)(App.Current)).subCtgHdr = gName;
        }

        public class SubCategoryClass
        {
            public string name { get; set; }
            public string code { get; set; }
        }

        private void MenuBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Menu));
        }

        private void SearchBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Search));
        }
    }
}
