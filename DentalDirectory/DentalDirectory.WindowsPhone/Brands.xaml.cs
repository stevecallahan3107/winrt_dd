﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace DentalDirectory
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Brands : Page
    {
        public MainModel Model { get; set; }
        public BrandClass Name { get; set; }
        public string gCode;
        public string gName;

        public Brands()
        {
            this.InitializeComponent();

            if (((App)(App.Current)).loggedIn)
            {
                UserBttn.Visibility = Visibility.Visible;
            }
            else
            {
                UserBttn.Visibility = Visibility.Collapsed;
            }

            this.NavigationCacheMode = NavigationCacheMode.Required;

            Model = new MainModel();
            DataContext = Model;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await Model.brandLoad(gCode);
        }

        private void subCtgList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1)
            {
                BrandApiModel brandSelected = (BrandApiModel)e.AddedItems[0];
                BrandClass brandDtl = new BrandClass();
                brandDtl.name = brandSelected.Name;
                brandDtl.code = gCode + "/" + brandSelected.Code;
                ((App)(App.Current)).brand_Name = brandDtl.name;
                ((App)(App.Current)).brand_Code = brandDtl.code;
                Frame.Navigate(typeof(BrandsDetail), brandDtl);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var ctgDetail = e.Parameter as SubCategories.SubCategoryClass;
            gCode = ctgDetail.code;
            gName = ctgDetail.name;
            HeaderTxt.Text = gName;
            ((App)(App.Current)).brandHdr = gName;
        }

        public class BrandClass
        {
            public string name { get; set; }
            public string code { get; set; }
        }

        private void MenuBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Menu));
        }

        private void SearchBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Search));
        }
    }
}
