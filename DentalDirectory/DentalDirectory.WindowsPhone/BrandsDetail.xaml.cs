﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace DentalDirectory
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BrandsDetail : Page
    {
        public MainModel Model { get; set; }
        public string gCode;
        public string gName;

        public BrandsDetail()
        {
            this.InitializeComponent();

            if (((App)(App.Current)).loggedIn)
            {
                UserBttn.Visibility = Visibility.Visible;
            }
            else
            {
                UserBttn.Visibility = Visibility.Collapsed;
            }

            this.NavigationCacheMode = NavigationCacheMode.Required;

            Model = new MainModel();
            DataContext = Model;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await Model.brandDtlLoad(gCode);
        }

        private void subCtgList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1)
            {
                BrandDetailApiModel brandSelected = (BrandDetailApiModel)e.AddedItems[0];
                BrandDtlClass brandDtl = new BrandDtlClass();
                brandDtl.name = brandSelected.Name;
                brandDtl.code = brandSelected.Code;
                ((App)(App.Current)).brandDtl_Name = brandDtl.name;
                ((App)(App.Current)).brandDtl_Code = brandDtl.code;
                Frame.Navigate(typeof(BrandItemDetail), brandDtl);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var brandDetail = e.Parameter as Brands.BrandClass;
            gCode = brandDetail.code;
            gName = brandDetail.name;
            HeaderTxt.Text = gName;
            ((App)(App.Current)).brandDtlHdr = gName;
        }

        public class BrandDtlClass
        {
            public string name { get; set; }
            public string code { get; set; }
        }

        private void MenuBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Menu));
        }

        private void SearchBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Search));
        }

    }
}
