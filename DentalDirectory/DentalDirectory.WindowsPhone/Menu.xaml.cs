﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace DentalDirectory
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Menu : Page
    {
        
        public Menu()
        {
            this.InitializeComponent();
            if (((App)(App.Current)).loggedIn)
            {
                UserBttn.Visibility = Visibility.Visible;
                logIn.Text = "Log Out";
            }
            else
            {
                UserBttn.Visibility = Visibility.Collapsed;
                logIn.Text = "Log In";
            }
            if (((App)(App.Current)).subCtgHdr != null)
            {
                subCtg.Visibility = Visibility.Visible;
                subCtg.Text = ((App)(App.Current)).subCtgHdr;
            }
            else
            {
                subCtg.Visibility = Visibility.Collapsed;
            }
            if (((App)(App.Current)).brandHdr != null)
            {
                brand.Visibility = Visibility.Visible;
                brand.Text = ((App)(App.Current)).brandHdr;
            }
            else
            {
                brand.Visibility = Visibility.Collapsed;
            }

            if (((App)(App.Current)).brandDtlHdr != null)
            {
                brandDtl.Visibility = Visibility.Visible;
                brandDtl.Text = ((App)(App.Current)).brandDtlHdr;
            }
            else
            {
                brandDtl.Visibility = Visibility.Collapsed;
            }

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Menu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1)
            {
                TextBlock sel = (TextBlock)e.AddedItems[0];
                if ((sel.Text == "Log In") || (sel.Text == "Log Out"))
                {

                }
                else if (sel.Text == "Categories")
                {
                    Frame.Navigate(typeof(MainPage));
                }
                else if (sel.Text == subCtg.Text)
                {
                    DentalDirectory.MainPage.CategoryClass categoryDtl = new DentalDirectory.MainPage.CategoryClass();
                    categoryDtl.name = ((App)(App.Current)).categoryDtl_Name;
                    categoryDtl.code = ((App)(App.Current)).categoryDtl_Code;
                    Frame.Navigate(typeof(SubCategories), categoryDtl);
                }
                else if (sel.Text == brand.Text)
                {
                    DentalDirectory.SubCategories.SubCategoryClass subCtgDtl = new DentalDirectory.SubCategories.SubCategoryClass();
                    subCtgDtl.name = ((App)(App.Current)).subCategoryDtl_Name;
                    subCtgDtl.code = ((App)(App.Current)).subCategoryDtl_Code;
                    Frame.Navigate(typeof(Brands), subCtgDtl);
                }
                else if (sel.Text == brandDtl.Text)
                {
                    DentalDirectory.Brands.BrandClass brandDetail = new DentalDirectory.Brands.BrandClass();
                    brandDetail.name = ((App)(App.Current)).brand_Name;
                    brandDetail.code = ((App)(App.Current)).brand_Code;
                    Frame.Navigate(typeof(BrandsDetail), brandDetail);
                } 

            }
        }

        private void MenuBttn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SearchBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Search));
        }
    }
}
