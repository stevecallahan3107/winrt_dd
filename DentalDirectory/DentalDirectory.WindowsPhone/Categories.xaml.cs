﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace DentalDirectory
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainModel Model { get; set; }
        bool IsVisible { get; set; }

        public MainPage()
        {
            this.InitializeComponent();
            ((App)(App.Current)).loggedIn = true;
            if (((App)(App.Current)).loggedIn)
            {
                UserBttn.Visibility = Visibility.Visible;
            }
            else
            {
                UserBttn.Visibility = Visibility.Collapsed;
            }

            this.NavigationCacheMode = NavigationCacheMode.Required;

            Model = new MainModel();
            DataContext = Model;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await Model.ctgLoad();
        }

        private void ctgList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 1)
            {
                CategoryApiModel ctgSelected = (CategoryApiModel)e.AddedItems[0];
                CategoryClass categoryDtl = new CategoryClass();
                categoryDtl.name = ctgSelected.Name;
                categoryDtl.code = ctgSelected.Code;
                ((App)(App.Current)).categoryDtl_Name = categoryDtl.name;
                ((App)(App.Current)).categoryDtl_Code = categoryDtl.code;
                Frame.Navigate(typeof(SubCategories), categoryDtl);
            }
        }

        public class CategoryClass
        {
            public string name { get; set; }
            public string code { get; set; }
        }

        private void MenuBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Menu));
        }

        private void SearchBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Search));
        }

        private void CartBttn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Login));
        }

        private async void UserBttn_Click(object sender, RoutedEventArgs e)
        {
             MessageDialog md = new MessageDialog("You are currently logged in as.", "Forgot Password");
            bool? result = null;
            md.Commands.Add(
               new UICommand("Submit", new UICommandInvokedHandler((cmd) => result = true)));
            md.Commands.Add(
               new UICommand("Cancel", new UICommandInvokedHandler((cmd) => result = false)));

            await md.ShowAsync();
            if (result == true)
            {
                //Run forget pwd async    
            }
        }

    }

}
